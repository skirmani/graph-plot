This is a Python utility to draw graphs or networks given their coordinates.

Required:
1. Python 2.7
2. Graph or Network in mtx format
3. X,Y coordinates in a csv file

Format for XYZ coordinates file:
If the graph has 3 vertices, then the coordinate file will have three lines 
with comma separated x and y coordinates

Example: 
python gplot.py apache1.mtx apache1.xyz 1000 1

Output: png file written in local directory

This tool was developed as part of the following publications:
1. Spectral graph drawing: Building blocks and performance analysis - https://ieeexplore.ieee.org/document/8425420
2. A Scalability and Sensitivity Study of Parallel Geometric Algorithms for Graph Partitioning - https://www.researchgate.net/publication/327980666_A_Scalability_and_Sensitivity_Study_of_Parallel_Geometric_Algorithms_for_Graph_Partitioning
3. Scalable parallel graph partitioning - https://dl.acm.org/citation.cfm?id=2503280

Please cite:

@INPROCEEDINGS{8425420, 
author={S. Kirmani and K. Madduri}, 

booktitle={2018 IEEE International Parallel and Distributed Processing Symposium Workshops (IPDPSW)}, 

title={Spectral Graph Drawing: Building Blocks and Performance Analysis}, 

year={2018}, 

volume={}, 

number={}, 

pages={269-277}, 

keywords={data visualisation;graph theory;spectral graph drawing;building blocks;performance analysis;three dimensional visualization;spectral graph theory;large-scale graphs;spectral drawings;two dimensional visualization;Layout;Laplace equations;Partitioning algorithms;Eigenvalues and eigenfunctions;Smoothing methods;Symmetric matrices;Graph theory;graph drawing;spectral;embedding},

doi={10.1109/IPDPSW.2018.00053},

ISSN={}, 

month={May},}


