import sys
argc = len(sys.argv)
if argc < 5:
	print("Usage: gplot.py <graphfile> <xyzfile> <num of pixels, say 1000 or 10000> <show(0/1)>")
	sys.exit(1)

#graph file
graphfile = sys.argv[1]
#xyz file
xyzfile = sys.argv[2]
#number of pixels
numPixel = int(sys.argv[3])
#show or no show
show = int(sys.argv[4])

#check input files
import os.path
pngfile = './graph_draw.png'
if not os.path.isfile(graphfile):
	print("Error: mtx file not available in relative path " + graphfile)
	sys.exit(1)
if not os.path.isfile(xyzfile):
	print("Error: xyz file not available in relative path " + xyzfile)
	sys.exit(1)

print("Info: png file will be writted at relative path " + pngfile)


#Read graph
from scipy.io import mmread
A = mmread(graphfile)
N = A.get_shape()[0]
NNZ = A.getnnz()
row = A.row
col = A.col
print("N=%d, NNZ=%d" % (N, NNZ))


#Read coordinates
import numpy as np
XY = np.loadtxt(xyzfile, delimiter=',')

minX = np.min(XY[0])
maxX = np.max(XY[0])

minY = np.min(XY[1])
maxY = np.max(XY[1])

#Normalize coordinates
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(copy=False)
_ = scaler.fit_transform(XY)


#Draw
from PIL import Image
from PIL import ImageDraw
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

#Scaling image to original dimensions
xrng = (int)(numPixel*(maxX-minX)/(maxY-minY)) 
s = (xrng, numPixel)

im = Image.new('RGBA', s, (255,255,255,255))
draw = ImageDraw.Draw(im)

#line width
line_width = numPixel/1000
if line_width < 1:
    line_width = 1

for i in range(NNZ):
    p1 = row[i]
    p2 = col[i]
    x1 = XY[p1,0] * s[0]
    y1 = XY[p1,1] * s[1]
    x2 = XY[p2,0] * s[0]
    y2 = XY[p2,1] * s[1]
    draw.line(((x1,y1),(x2,y2)), fill=(0, 0, 255, 255), width=int(line_width))

plt.imshow(np.asarray(im), extent=None, aspect='auto', origin='lower')
if show == 1:
	plt.show()
im.save(pngfile, 'PNG')
